import datetime
import requests
import sys
import csv


class WeatherForecast:
    def __init__(self, api_key, city):
        self.city = city
        self.api_key = api_key
        self.cached = {}
        self.read_cached_weather_from_file()

    def __iter__(self):
        for date in self.cached.keys():
            yield date

    def __getitem__(self, date):
        return self.cached.get(date, f"Nie znam pogody dla {date}")

    def check_weather(self, date):
        if date in self.cached:
            print(f"Pogoda dla {date} w pamieci cached.")
            print(f"Prognoza na dzien {date} to {self.cached[date]}")
            return

        url = "https://weatherapi-com.p.rapidapi.com/forecast.json"
        querystring = {"q": self.city, "dt": date}
        headers = {
            'x-rapidapi-host': "weatherapi-com.p.rapidapi.com",
            'x-rapidapi-key': self.api_key
        }
        response = requests.request("GET", url, headers=headers, params=querystring)

        if response.status_code != 200:
            print("Błąd nie pobrano danych pogody")
            return

        forecast_data = response.json()
        self.cached[date] = forecast_data['current']['condition']['text']

        self.save_to_cache_file([date, forecast_data['current']['condition']['text']])
        self.check_if_raining(forecast_data['current']['condition']['text'])

    def save_to_cache_file(self, data):
        with open("cached_weather_file.csv", "a") as f:
            csv_writer = csv.writer(f)
            csv_writer.writerow(data)

    def check_if_raining(self, condition):
        keyword = ['rain', "drizzle"]
        if keyword[0] in condition or keyword[1] in condition:
            print("Bedzie padać")
        else:
            print("Nie bedzie padac")

    def items(self):
        for date, weather in self.cached.items():
            yield date, weather

    def read_cached_weather_from_file(self):
        with open("cached_weather_file.csv") as f:
            csv_reader = csv.DictReader(f)
            for line in csv_reader:
                self.cached[line["date"]] = line["weather"]


def main():
    args = sys.argv[1:]
    klucz = args[0]
    data = args[1] if len(args) > 1 else (datetime.date.today() + datetime.timedelta(days=1)).strftime("%Y-%m-%d")
    wf = WeatherForecast(klucz, "Warsaw")
    wf.check_weather("2022-01-25")
    wf.check_weather("2022-01-27")
    wf.check_weather("2022-01-28")
    print(wf["2022-01-24"])
    print(list(wf.items()))

    for d in wf:
        print(d)
    wf.check_weather(data)
    print(wf["2022-01-24"])
    print(list(wf.items()))


main()

